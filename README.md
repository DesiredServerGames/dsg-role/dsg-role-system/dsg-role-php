# Ansible Role:

Install and configure PHP 7.4 in RHEL/Fedora, Debian/Ubuntu.

## Requirements

None

## Role Variables

None

## Dependencies

None
## Example Playbook

    - hosts: servers
      roles:
         - { role: dsg-role-php }

## License

MIT

## Author Information

This role was created in 2021 by Didier MINOTTE.